package com.silvioapps.mqttsample;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements MqttCallbackExtended{
    private static final String USERNAME = "pykswtkr";
    private static final String PASSWORD = "8obiEj8vq-Rn";
    public static final String SERVER_ADDRESS = "tcp://201.64.6.82:1883";//"tcp://m20.cloudmqtt.com:18744";//"tcp://broker.mqttdashboard.com:1883";//;//
    public static final String SUBSCRIPTION_TOPIC = "office";
    public static final String PUBLISH_TOPIC = "office";
    public static final String TOPIC_STATUS = "status";
    public static final int DELAY_TIMER = 1000;
    public static final int PERIOD_TIMER = 10000;
    public static final int TIMEOUT_TIMER = 100000;
    public static final String USER_TAG = "&u=";
    public static final String TIME_TAG = "&t=";
    public static final String MESSAGE_TAG = "&m=";

    private String username = "unknown";
    private MqttAndroidClient mqttAndroidClient = null;
    private String message = null;
    private EditText messageEditText = null;
    private Button sendMessageButton = null;
    private EditText usernameEditText = null;
    private TextView resultTextView = null;
    private Button connectButton = null;
    private MqttConnectOptions mqttConnectOptions = null;
    private long lastTimeSeen = 0;
    private HashMap<String, Long> userHashMap = null;
    private ArrayList<String> userList = null;
    private View sendMessageLayout = null;
    private MyNotification notification = null;
    private Thread mainThread = null;
    private boolean out = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userHashMap = new HashMap<>();
        userHashMap.clear();
        userList = new ArrayList<>();
        userList.clear();

        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        messageEditText = (EditText) findViewById(R.id.messageEditText);

        sendMessageButton = (Button) findViewById(R.id.sendMessageButton);
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (messageEditText != null) {
                    message = messageEditText.getText().toString();

                    publishMessage(username, ""+System.currentTimeMillis(), message, PUBLISH_TOPIC);
                }
            }
        });

        sendMessageLayout = findViewById(R.id.sendMessageLayout);

        resultTextView = (TextView) findViewById(R.id.resultTextView);

        connectButton = (Button) findViewById(R.id.connectButton);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usernameEditText != null) {
                    username = usernameEditText.getText().toString();
                }

                mqttAndroidClient = new MqttAndroidClient(getApplicationContext(), SERVER_ADDRESS, username);
                mqttAndroidClient.setCallback(MainActivity.this);

                connect();

                if(connectButton.getText().equals("Disconnect")) {
                    disconnect();
                }
            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask()
        {
            public void run()
            {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String time = "" + System.currentTimeMillis();

                        publishStatus(username, time, TOPIC_STATUS);

                        setWill();

                        if(userList != null && userHashMap != null) {
                            for (int i = 0; i <userList.size();i++) {
                                String user = userList.get(i);
                                long time_ = userHashMap.get(user);

                                if (time_ > 0 && System.currentTimeMillis() - time_ > TIMEOUT_TIMER) {
                                    Log.i("TAG_MQTTT", "user "+user+" status OFFLINE");
                                }
                            }
                        }
                    }
                });
            }
        }, DELAY_TIMER, PERIOD_TIMER);


        Intent intent = new Intent(this, MainActivity.class);
        notification = new MyNotification(intent,this);
    }

    @Override
    public void connectComplete(boolean reconnect, String serverURI) {
        if (reconnect) {
            Toast.makeText(MainActivity.this, "Reconnected to : "+SERVER_ADDRESS,Toast.LENGTH_LONG).show();

            subscribeToTopic(SUBSCRIPTION_TOPIC);

            subscribeToTopic(TOPIC_STATUS);
        } else {
            Toast.makeText(MainActivity.this, "Connected to : "+SERVER_ADDRESS,Toast.LENGTH_LONG).show();
        }

        if(sendMessageLayout != null) {
            sendMessageLayout.setVisibility(View.VISIBLE);
        }

        if(connectButton != null){
            connectButton.setText("Disconnect");
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        if (sendMessageLayout != null){
            sendMessageLayout.setVisibility(View.GONE);
        }

        if(connectButton != null){
            connectButton.setText("Connect");
        }

        Toast.makeText(MainActivity.this, "The Connection was lost.",Toast.LENGTH_LONG).show();
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        if(topic.equals(TOPIC_STATUS)){
            String message = new String(mqttMessage.getPayload());
            String user = message.substring(message.indexOf(USER_TAG)+USER_TAG.length(), message.indexOf(TIME_TAG));
            String time = message.substring(message.indexOf(TIME_TAG)+TIME_TAG.length());

            try {
                lastTimeSeen = Long.parseLong(time);
            }
            catch(NumberFormatException e){
                e.printStackTrace();
            }

            if(userHashMap != null && userList != null){
                if(!userHashMap.containsKey(user) && !userList.contains(user)) {
                    userHashMap.put(user, (long) 0);
                    userList.add(user);
                }
                else{
                    userHashMap.put(user, lastTimeSeen);
                    userList.add(user);
                }
            }
        }
        else if(topic.equals(PUBLISH_TOPIC)){
            String message = new String(mqttMessage.getPayload());
            String user = message.substring(message.indexOf(USER_TAG)+USER_TAG.length(), message.indexOf(TIME_TAG));
            String time = message.substring(message.indexOf(TIME_TAG)+TIME_TAG.length(), message.indexOf(MESSAGE_TAG));
            String message_ = message.substring(message.indexOf(MESSAGE_TAG)+MESSAGE_TAG.length());

            if(notification != null) {
                notification.createNotification(user, time, message_);
                notification.startNotification();
            }

            //Toast.makeText(MainActivity.this, "Incoming message: "+message_ +"  from topic "+topic,Toast.LENGTH_LONG).show();

            if(resultTextView != null){
                resultTextView.append("Message from = "+ user + " time = " + time + " msg = "+ message_ + "\n");
            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        //Toast.makeText(MainActivity.this, "Message sent succesfully!",Toast.LENGTH_LONG).show();
    }

    public void connect(){
        if(mqttAndroidClient != null && !mqttAndroidClient.isConnected()) {
            mqttConnectOptions = new MqttConnectOptions();
            mqttConnectOptions.setAutomaticReconnect(true);
            mqttConnectOptions.setCleanSession(false);
            mqttConnectOptions.setUserName(USERNAME);
            mqttConnectOptions.setPassword(PASSWORD.toCharArray());
            mqttConnectOptions.setConnectionTimeout(60);

            try {
                Toast.makeText(MainActivity.this, "Connecting to: " + SERVER_ADDRESS, Toast.LENGTH_LONG).show();
                mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        subscribeToTopic(SUBSCRIPTION_TOPIC);

                        subscribeToTopic(TOPIC_STATUS);
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Toast.makeText(MainActivity.this, "Failed to connect to: " + SERVER_ADDRESS, Toast.LENGTH_LONG).show();
                    }
                });
            } catch (MqttException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void disconnect(){
        if(mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
            try {
                mqttAndroidClient.disconnect(0);
            }
            catch(MqttException e){
                e.printStackTrace();
            }
        }
    }

    public void subscribeToTopic(String topic){
        try {
            if(mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
                mqttAndroidClient.subscribe(topic, 0, null, new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        Toast.makeText(MainActivity.this, "Subscribed!", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Toast.makeText(MainActivity.this, "Failed to subscribe", Toast.LENGTH_LONG).show();
                    }
                });
            }

        } catch (MqttException ex){
            ex.printStackTrace();
        }
    }

    public void publishMessage(String userId, String timeMillis,String message, String topic){
        try {
            String message_ = USER_TAG+userId+TIME_TAG+timeMillis+MESSAGE_TAG+message;



            Log.i("TAG2","message_ "+message_);



            //MqttMessage mqttMessage = new MqttMessage();
           // mqttMessage.setPayload(message_.getBytes());

            if(mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
                mqttAndroidClient.publish(topic, message_.getBytes(), 0 , true);

                //Toast.makeText(MainActivity.this, "Message Published", Toast.LENGTH_LONG).show();
                if (!mqttAndroidClient.isConnected()) {
                    //Toast.makeText(MainActivity.this, mqttAndroidClient.getBufferedMessageCount() + " messages in buffer.", Toast.LENGTH_LONG).show();
                }
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void publishStatus(String userId, String timeMillis, String topic){
        try {
            String message = USER_TAG+userId+TIME_TAG+timeMillis;

            MqttMessage mqttMessage = new MqttMessage();
            mqttMessage.setPayload(message.getBytes());

            if(mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
                mqttAndroidClient.publish(topic, message.getBytes(), 0 , true);
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void setWill(){
        String message = USER_TAG+username+TIME_TAG+System.currentTimeMillis();

        if(mqttConnectOptions != null) {
            mqttConnectOptions.setWill(TOPIC_STATUS, message.getBytes(Charset.defaultCharset()), 0, true);
        }
    }
}
